# Programación declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)
Por Gonzalo Arroyo Julio, profesor del Departamento de Lenguajes y Sistemas Informáticos UNED.

Redactora - Locutora: Ventureira Pedrosa Ana Isabel CEMAV UNED.

```plantuml
@startmindmap
+[#yellow] Programación
++_ es
+++[#lightgray] indicarle a la máquina lo que se requiere hacer
++[#lightgreen] surgimiento
+++[#lightblue] derivada de problemas en programación clásica
++[#lightgreen] tipos
+++[#lightgray] orientada a la IA
++++ tipos de lenguajes
+++++[#lightyellow] estándar
++++++[#white] HASKELL
+++++++_ utilizado en
++++++++[#lightpink] sistemas diferenciales
+++++++_ tiene
++++++++[#lightpink] evolución perezosa
++++++[#white] PROLOG
+++++++_ representa
++++++++[#lightpink] paradigma declarativo
+++++[#lightyellow] híbrido
++++++[#white] LISP
+++++++_ permite
++++++++[#lightpink] hacer un tipo de programación imperativa
+++++++_ usado en
++++++++[#lightpink] programación funcional
+++[#lightblue] imperativa
++++_ es
+++++[#orange] programación más antigua
+++++[#lightpink] idea abstracta
+++++[#orange] asume que la máquina es tonta
++++_ busca
+++++[#lightpink] alternativas
++++[#lightyellow] tipos
+++++[#orange] modular
+++++[#lightpink] estructurada
+++++[#orange] procedimental
++++_ lenguajes
+++++[#lightpink] FORTRAN
+++++[#orange] C
+++++[#lightpink] PASCAL
+++[#lightgray] operativa
++++_ está basada en
+++++[#orange] Modelo de Von Neumann
++[#lightgreen] visualización
+++[#lightblue] burocrática
++++[#lightyellow] secuencia de órdenes
+++++_ para
++++++[#lightpink] ejecutar ideas
++++[#lightyellow] código máquina
++++[#lightyellow] gestón de memoria
+++[#lightgray] creativa
++++_ es
+++++[#orange] ideas
+++++[#lightpink] forma de programar
+++++[#orange] creación de algoritmos
++[#lightgreen] evolución
+++_ basada anteriormente en
++++[#lightblue] lenguaje máquina
+++_ consistían en
++++[#lightgray] secuencia de órdenes
+++++_ de tipo
++++++[#lightpink] sencillas
++++++[#lightpink] concretas
++++++[#lightpink] directas
++[#lightgreen] programación declarativa
+++_ ¿qué es?
++++[#lightyellow] paradigma de programación
+++++_ basado en
++++++_ invulnerablilidad a
+++++++[#lightpink] cuello de botella
++++++[#orange] detalles de cálculo
++++++[#lightpink] asignación de memoria
++++++[#orange] resultado deseado
++++++_ ejemplos
+++++++[#lightpink] PROLOG
+++++++[#lightpink] HASKELL
+++++++[#lightpink] ERLANG
+++++++[#lightpink] MIRANDA
+++++++[#lightpink] LISP
+++_ basada en
++++[#lightpink] afirmaciones y propósitos
++++[#orange] inexistencia de variables
++++[#lightpink] funciones o expresiones
++++[#orange] transparencia referencial
+++_ consiste en
++++[#lightpink] enfásis en los aspectos creativos para lograr resolver el problema
++++[#orange] estilo de programación
++++[#lightpink] abandono de secuencias de órdenes que gestionan la memoria del ordenador
+++[#lightblue] objetivos
++++[#orange] reducción en la complejidad de los programas
++++[#orange] evitar riesgos de cometer errores al hacer programas
++++[#lightpink] liberarse de asignaciones
++++[#orange] detallar el control de la gestión de memoria en el ordenador
++++[#lightpink] fácil depuración
++++[#orange] utilización de recursos que permitan especificar programas a un nivel más alto
+++[#lightgray] tipos
++++[#lightyellow] programación lógica
+++++[#white] relaciones entre objetos definidos
+++++[#white] modelo de demostración lógica
++++++[#lightpink] expresiones de programas
+++++++[#orange] compilador
++++++++[#lightgray] motor de inferencia
++++++++[#lightgray] da respuesta a las preguntas
+++++++[#orange] axiomas
++++++++[#lightgray] conjunto de hechos y reglas de inferencia
++++++[#lightpink] lógica de predicados
++++++_ ventajas
+++++++[#lightpink] permite ser más declarativo
+++++++[#lightpink] establece dirección de procesamiento mediante argumentos y resultados
++++++_ ejemplo
+++++++[#lightpink] demostración de predicados de un sistema
++++[#lightyellow] programación funcional
+++++[#white] usa el modelo de los matemáticos
++++++_ a través de
+++++++[#lightgray] reglas de reescritura
+++++++[#lightgray] simplificación de expresiones
+++++++[#lightgray] datos de entrada y salida
+++++++[#lightgray] aplicación de funciones a datos complejos
+++++_ características
++++++[#lightpink] mejora el razonamiento
++++++[#lightpink] especificación de programas
+++++[#white] ventajas
++++++[#lightgray] evalucación perezosa
++++++[#lightgray] datos infinitos
+++++++_ como
++++++++[#orange] FINONACCI
++++++[#lightgray] evalúa aquello que es necesario
+++++_ desarrollo de
++++++[#lightpink] IA
++++++[#lightpink] algoritmos
++++++[#lightpink] compiladores y analizadores
+++[#lightblue] desventajas
++++[#lightgray] más complejo
++++[#lightgray] difícil comprensión
++++[#lightgray] forma no habitual para las personas
@endmindmap
```

# Lenguaje de Programación Funcional (2015)
Por Gonzalo Arroyo Julio, profesor del Departamento de Lenguajes y Sistemas Informáticos UNED.

Redactora - Locutora: Ventureira Pedrosa Ana Isabel CEMAV UNED.

```plantuml
@startmindmap
+[#yellow] Programación funcional
++[#lightblue] surgimiento
+++[#lightyellow] cálculo lambda
++++_ es
+++++[#lightgray] un sistema computacional potente
++++_ introducido por
+++++[#lightgreen] Alonzo Church
+++++[#lightgreen] Stephen Kleene
++++_ año de creación
+++++[#lightgray] 1930
++++_ objetivos
+++++[#lightgray] recursión
+++++[#lightgray] noción de aplicaciones de funciones
+++[#white] Von Neumann
++++[#lightgray] instrucciones ejecutándose
++++[#lightgray] instrucciones de asignación
++++[#lightgray] lenguaje imperativo
+++++_ como
++++++[#lightgreen] Programación orientada a objetos
++[#lightblue] evaluación
+++[#lightyellow] estricta
++++[#lightgray] evalúa lo interno antes de lo externo
+++[#white] no estricta
++++[#lightgreen] dificultad al interpretar
++++[#lightgreen] evalúa lo externo antes que lo interno
+++[#lightyellow] perezosa
++++[#lightgray] retraza el cálculo de una expresión
++++[#lightgray] no repite la evaluación a menos que sea necesaria
+++[#white] de corto circuito
++++[#lightgray] trabaja con operadores booleanos
++++_ funciones
+++++[#lightgreen] el primer argumento de la función QR evalúa si el resultado es verdadero el valor total es igual
+++++[#lightgreen] el segundo argumento no se ejecuta si el primero, de la función AND, evalúa, entonces el resultado es falso
++[#lightblue] paradigmas
+++[#lightyellow] primeros ordenadores
++++[#lightgray] dotan de semántica los programas
++++[#lightgray] modelo de computación de Von Neumann
+++[#white] lógico
++++[#lightgray] lógica simbólica
+++++_ es
++++++[#lightgreen] conjunto de sentencias con respecto a un programa
+++[#lightyellow] imperativo
++++[#lightgray] POO
+++++_ son
++++++[#lightgreen] pequeños trozos de códigos llamados objetos
+++++++_ compuesto de
++++++++[#lightpink] instrucciones y se ejecuta secuencialmente
++[#lightblue] memorización
+++_ surge en
++++[#lightgreen] 1968 por Donald Michie
+++[#lightgray] almacena el valor de una expresión de la evaluación realizada
++[#lightblue] currificación
+++_ es una
++++[#lightgray] función en homenaje a Haskell Brooks que recibe varias funciones de entrada y devuelve diferentes salidas
++[#lightblue] características
+++[#lightyellow] el orden es irrelevante
+++[#white] todo gira en torno a funciones
++++[#lightgray] funciones constantes
+++++[#lightpink] devuleven siempre el mismo resultado
++++[#lightgray] funciones de orden superior
+++++_ efectúa
++++++[#lightgreen] tomar varias funciones de entrada
++++++[#lightgreen] devolver una de salida
++++[#lightgray] funciones matemáticas
+++++_ consecuencias
++++++[#lightgreen] recursión
++++++[#lightgreen] concepto de asignación
++++++_ desaparece el concepto de
+++++++[#lightpink] variable
++++++++[#orange] parámetros de entrada de las funciones
+++++++[#lightpink] bucle
++++++++[#orange] funciones recursivas
+++[#lightyellow] transferencia referencial
++[#lightblue] ventajas
+++[#white] código corto, preciso y verificable
+++[#white] estrategia para programar
+++[#white] procesa tareas computacionales
++[#lightblue] desventajas
+++[#lightyellow] no existe la asignación
+++[#lightyellow] no existen bucles
++++_ ejemplos
+++++[#orange] Finonacci
+++++[#orange] Factorial
++[#lightblue] aplicaciones
+++[#lightgray] juegos de arcade
+++[#lightgray] programar videojuegos
+++[#lightgray] IA
+++[#lightgray] algoritmos
+++[#lightgray] compiladores y analizadores
++[#lightblue] lenguajes
+++[#white] LISP
+++[#white] HASKELL
+++[#white] ERLANG
+++[#white] SCALA
+++[#white] F#
@endmindmap
```